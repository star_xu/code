#ifndef __UTILS_H__
#define __UTILS_H__
int* array_init(int count);
int check_array(int* p, int count);
void array_free(int *p);
int array_output(int* p, int count);
#endif