#include <stdio.h>

enum Color {
    white = 0,
    gray = 1,
    black = 2
};
enum Station {
    Start = 0xf,
    Cross = 0xf0,
    End = 0xf00
};
struct Node {
    int id;
    int color;
    struct Edge *first;
    int station; // start cross end
    unsigned int d;
    unsigned int cross_num;
    int index;
};
struct Edge {
    unsigned int weight;
    int u;
    int v;
    struct Edge *next;
};
static int node_num;
int locate(struct Node *nodes, int id)
{
    for (int i = 0; i < node_num; i++) {
        if (nodes[i].id == id)
            return i;
    }
    return -1;
}
struct Stack {
    int top;
    int *data;
    int cross_num;
    int distance;
};
static struct Stack s;
unsigned int min = 0xffffffff;
void push(struct Node *nodes, int u)
{
    s.data[s.top] = u;
    s.top++;
    if ((nodes[u].station & Cross) == Cross)
        s.cross_num++;
}
void pop(struct Node *nodes)
{
    s.top--;
    int u = s.data[s.top];
    if ((nodes[u].station & Cross) == Cross)
        s.cross_num--;
}
int cross_num;
void dfs(struct Node *nodes, int u0)
{
    struct Node *u;
    struct Node *v;
    u = &nodes[u0];
    u->color = gray;
    for (struct Edge *e = u->first; e != NULL; e=e->next) {
        v = &nodes[e->v];
        if ((v->station & End) == End) {
            if (u->cross_num == cross_num) {
                if (u->d + e->weight < min)
                    min = u->d + e->weight;
            }
            continue;//当遍历到end 点， 应该继续遍历下一个边，而不是return
        }
        if (v->color == white) {
            v->d = u->d + e->weight;
            if ((v->station & Cross) == Cross)
                v->cross_num = u->cross_num + 1;
            else
                v->cross_num = u->cross_num;
            dfs(nodes, e->v);
            v->color = white;
            v->d = 0;
            v->cross_num = 0;
        }
    }
}
int main()
{
    int n = 0;;
    int id1 = 0;
    int id2 = 0;
    int d = 0;
    int u = 0;
    int v = 0;
    scanf("%d", &n);
    struct Node *nodes = malloc(sizeof(struct Node) * n);
    memset(nodes, 0, sizeof(struct Node));
    for (int i = 0; i < n; i++) {
        scanf("%d%d%d", &id1, &id2, &d);
        u = locate(nodes, id1);
        if (u == -1) {
           u = node_num;
           node_num++;
           nodes[u].id = id1;
           nodes[u].index = u;
        }
        v = locate(nodes, id2);
        if (v == -1) {
           v = node_num;
           node_num++;
           nodes[v].id = id2;
           nodes[v].index = v;
        }
        struct Edge *e = malloc(sizeof(struct Edge));
        memset(e, 0, sizeof(struct Edge));
        e->next = nodes[u].first;
        nodes[u].first = e;
        e->u = u;
        e->v = v;
        e->weight = d;

        e = malloc(sizeof(struct Edge));
        memset(e, 0, sizeof(struct Edge));
        e->next = nodes[v].first;
        nodes[v].first = e;
        e->u = v;
        e->v = u;
        e->weight = d;
    }
    int startid;
    int endid;
    int start_index;
    scanf("%d", &startid);
    u = locate(nodes, startid);
    if (u < 0) {
        printf("find start id %d error\n", startid);
        return 0;
    }
    start_index = u;
    nodes[u].station |= Start;
    nodes[u].d = 0;
    nodes[u].cross_num = 0;
    scanf("%d", &endid);
    v = locate(nodes, endid);
    if (v < 0) {
        printf("find endid %d error\n", endid);
        return 0;
    }
    nodes[v].station |= End;

    scanf("%d", &cross_num);
    for (int i = 0; i < cross_num; i++) {
        int cross_id;
        scanf("%d", &cross_id);
        int u = locate(nodes, cross_id);
        if (u < 0) {
            printf("find cross id %d error\n", cross_id);
            return 0;
        }
        nodes[u].station |= Cross;
    }
    dfs(nodes, start_index);
    printf("%d\n", min);
    for (int i = 0; i < node_num; i++) {
        printf("%d[%x]",nodes[i].id, nodes[i].station);
        for (struct Edge *e = nodes[i].first; e != NULL; e = e ->next) {
            printf("->%d[%x]:%d",nodes[e->v].id, nodes[e->v].station, e->weight);
        }
        printf("\n");
    }
    printf("Hello 711\n");
}
#if 0
6
1 2 2000
1 3 2000
1 4 4000
2 5 1000
3 4 1000
4 5 500
1
1
2
4
5
#endif