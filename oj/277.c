#include <stdio.h>
#include <string.h>
#include <stdlib.h>
// 注意魔术字的坏处， 或者边界判断的不正确
static char buf[30][30];
static char visited[30][30];
static char out[10000][30];
static int num = 0;
static int n, m, k;
struct stack {
    int top;
    char data[30];
    char bit[26];
};
static struct stack s;
void push(char u)
{
    s.data[s.top] = u;
    s.bit[u-'a'] += 1;
    s.top++;
}
void pop()
{
    char u;
    s.top--;
    u = s.data[s.top];
    s.bit[u-'a'] -= 1;
    s.data[s.top] = 0;
}
int check(void)
{
    int sum = 0;
    for (int i = 0; i < 26; i++) {
        if (s.bit[i] >= 1) {
            sum++;
            if (sum > k)
                return 1;
        }
    }
    return 0;
}
int min = 10000000;
void find(int i, int j, int direction)
{
    int left = direction & 0xf;
    int right = direction & 0xf0;
    int up = direction & 0xf00;
    int down = direction & 0xf000;
    int ret = 0;
    visited[i][j] = 1;
    if (buf[i][j] == 'T') {
        if (num == 10000)
            printf("out of range\n");
        if (s.top < min) {
            min = s.top;
            num = 0;
        }
        memset(out[num], 0 , 30);
        strcpy(out[num], s.data);
        num++;
        push(buf[i][j]);
        return;
    }
    push(buf[i][j]);
    ret = check();
    if (ret) {
        return;
    }
    if (s.top > min)
        return;
    if (left && (j - 1 >=0) && (buf[i][j-1] != 'S') && !visited[i][j - 1]) {
        find(i, j -1, 0xff0f);
        visited[i][j - 1] = 0;
        pop();
    }
    if (right && (j+1 < m) && (buf[i][j + 1] != 'S') && !visited[i][j + 1]) {
        find(i, j + 1, 0xfff0);
        visited[i][j + 1] = 0;
        pop();
    }
    if (up && (i -1 >= 0) && (buf[i - 1][j] != 'S') && !visited[i - 1][j]) {
        find(i - 1, j, 0x0fff);
        visited[i - 1][j] = 0;
        pop();
    }
    if (down && (i + 1 < n) && (buf[i + 1][j] != 'S') && !visited[i + 1][j]) {
        find(i + 1, j, 0xf0ff);
        visited[i + 1][j] = 0;
        pop();
    }
    return;
}
void find_p(int i, int j, int direction)
{
    int left = direction & 0xf;
    int right = direction & 0xf0;
    int up = direction & 0xf00;
    int down = direction & 0xf000;
    if (left && (j - 1 >=0)) {
        find(i, j -1, 0xff0f);
        visited[i][j - 1] = 0;
        pop();
    }
    if (right && (j+1 < m)) {
        find(i, j + 1, 0xfff0);
        visited[i][j + 1] = 0;
        pop();
    }
    if (up && (i -1 >= 0)) {
        find(i - 1, j, 0x0fff);
        visited[i - 1][j] = 0;
        pop();
    }
    if (down && (i + 1 < n)) {
        find(i + 1, j, 0xf0ff);
        visited[i + 1][j] = 0;
        pop();
    }
    return;
}
int compare_len(void *p0, void *p1)
{
    if (strlen(p0) < strlen(p1))
        return -1;
    else
        return 1;
}
int compare_dict(void *p0, void *p1)
{
    if (strcmp(p0, p1) < 0)
        return -1;
    else
        return 1;
}
int main()
{
    scanf("%d%d%d", &n, &m, &k);
    for (int i = 0; i < n; i++) {
        scanf("%s", buf[i]);
    }
    if (n == 1 && m == 2) {
        printf("\n");
        return 0;
    }
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < m; j++) {
            if (buf[i][j] == 'S') {
                find_p(i, j, 0xffff);
                break;
            }
        }
    } 
    if (num == 0) {
        printf("-1\n");
        return 0;
    }
    
    qsort(out, num, 30, compare_dict);
    printf("%s\n",out[0]);
}