#include <stdio.h>
#include <string.h>
#include <malloc.h>
#include <stdlib.h>
#include <time.h>
#include "utils.h"

int* array_init(int count)
{
    int *p = malloc(count * sizeof(int));
    if (!p) {
        printf("malloc memory fail,Maybe there is no memory\n");
        return NULL;
    }
    memset(p, 0, count * sizeof(int));
    srand((int)time(0));
    for (int i = 0;i < count; i++)
        p[i] = rand()%100;
    return p;
}
int check_array(int* p, int count)
{
    for (int i = 0; i < count - 1; i++) {
        if (p[i] > p[i+1]) {
            printf("sort error\n");
            break;
        }
    }
    return 0;
}
int array_output(int* p, int count)
{
    printf("After insert sort the input array is:\n");
    for (int i = 0; i< count; i++)
        printf("%d ",p[i]);
    printf("\n");
}
void array_free(int *p)
{
    free(p);
}