#include <stdio.h>
#include <malloc.h>
#include <string.h>
#define N 26
#define M 500
enum Color {
    white = 0,
    gray,
    black
};
struct Node {
    char vertex;
    int indegree;
    int index;
    int color;
};
struct Graph {
    struct Node node[N];
    char matrix[M][M];
    int node_num;
    int edge_num;
    int num;
    int cycle;
    int not_sure;
};
struct stack {
    char data[N];
    int top;
};
static struct stack s;
void push(char u)
{
    s.data[s.top] = u;
    s.top++;
}
char pop()
{
    if (s.top == 0)
        return 0;
    s.top--;
    return s.data[s.top];
}
struct Node *find_first_node(struct Graph *g, int *sum)
{
    struct Node *u = NULL;
    for (int i = 0; i < N; i++) {
        if (g->node[i].vertex != 0 && g->node[i].indegree == 0) {
            if (!u)
                u = &g->node[i];
            (*sum)++;
        }
    }
    return u;
}
void delete_node(struct Graph *g, struct Node *u)
{
    g->node[u->index].vertex = 0;
    for (int j = 0; j < M; j++) {
        if (g->matrix[u->index][j] == 1) {
            g->node[j].indegree--;
        }
    }
    g->node_num--;
    g->num--;
}
static char out[27];
void dfs_visit(struct Graph *g, struct Node *u)
{
    u->color = gray;
    for (int j = 0; j < g->node_num; j++) {
        if (g->matrix[u->index][j] == 1) {
            struct Node *v = &g->node[j];
            if (v->color == white) {
                dfs_visit(g, v);
            } else if (v->color == gray) {
                g->cycle = 1;
                return;
            }
        }
    }
    u->color = black;
}
void dfs(struct Graph *g)
{
    struct Node *u;
    for (int i = 0; i < g->node_num; i++) {
        u = &g->node[i];
        if (u->color == white && u->vertex != 0)
            dfs_visit(g, u);
    }
}
int topo(struct Graph *g)
{
    struct Node *u;
    int sum;
    int i = 0;
    while(g->num) {
        sum = 0;
        u = find_first_node(g, &sum);
        if (u == NULL) {
                return 1; // have cycle;
        } else {
            dfs(g);
            if (g->cycle)
                return 1; // have cycle;
            if (sum > 1)
                return 2; // not determin
        }
        out[i++] = u->vertex;
        delete_node(g, u);
    }
    if (g->node_num > g->num) {
                return 2; // not determin
    }
    return 0;
}
#define BUF_SIZE 4
static char buf[BUF_SIZE];
static struct Graph temp;
int main()
{
    struct Graph *g = malloc(sizeof(struct Graph));
    if (g == NULL)
        return -1;
    memset(g, 0, sizeof(struct Graph));

    printf("please input node num and edge num:\n");
    scanf("%d%d", &g->node_num, &g->edge_num);
    for(int i = 0; i < g->edge_num; i++) {
        int u, v;
        scanf("%s", buf);
        u = buf[0] - 'A';
        v = buf[2] - 'A';
        if (g->node[u].vertex == 0) {
            g->node[u].vertex = buf[0];
            g->node[u].index = u;
            g->num++;
        }
        if (g->node[v].vertex == 0) {
            g->node[v].vertex = buf[2];
            g->node[v].index = v;
            g->num++;
        }
        g->node[v].indegree += 1;
        g->matrix[u][v] = 1;
        if (g->matrix[v][u] == 1) {
            printf("Inconsistency found after %d relations.\n", (i + 1));
            return 0;
        }
        memcpy(&temp, g, sizeof(struct Graph));
        int ret = topo(&temp);
        if (ret == 1) {
            printf("Inconsistency found after %d relations.\n", (i + 1));
            return 0;
        } else if (ret == 2) {
            continue;
        } else {
                printf("Sorted sequence determined after %d relations: ", (i + 1));
                printf("%s", out);
                printf(".\n");
                return 0;
        }
    }
    printf("Sorted sequence cannot be determined.\n");
    return 0;
}