#include "insert_sort.h"

void insert_sort(int *p, int count)
{
    int i, j;
    for(j = 1; j < count; j++) {
        int key = p[j];
        for(i = j-1; i >= 0; i--) {
            if (p[i] > key) {
                p[i+1] = p[i];
            } else {
                break;
            }
        }
        p[i+1] = key;
    }
}