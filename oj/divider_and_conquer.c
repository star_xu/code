#include "divider_and_conquer.h"
#include <stdio.h>
struct result {
    int low;
    int high;
    int sum;
};
static struct result sum_cross_sub_array(int *A, int low, int mid, int high)
{
    int left_sum = -0xffff;
    int sum = 0;
    int max_left = 0;
    for (int i = mid; i >= low; i--) {
        sum = sum + A[i];
        if (sum > left_sum) {
            left_sum = sum;
            max_left = i;
        }
    }
    sum = 0;
    int right_sum = -0xfffff;
    int max_right;
    for (int i = mid + 1; i <= high; i++) {
        sum = sum + A[i];
        if (sum > right_sum) {
            right_sum = sum;
            max_right = i;
        }
    }
    struct result res;
    res.low = max_left;
    res.high = max_right;
    res.sum = left_sum + right_sum;
    return res;
}
static struct result sum_array(int *A, int low, int high)
{
    struct result res;
    int mid;
    struct result res_left;
    struct result res_right;
    struct result res_cross;
    if (low == high) {
        res.low = low;
        res.high = high;
        res.sum = A[low];
        return res;
    } else {
        mid = (low + high)/2;
        res_left = sum_array(A, low, mid);
        res_right = sum_array(A, mid + 1, high);
        res_cross = sum_cross_sub_array(A, low, mid, high);
        if (res_left.sum >= res_cross.sum && res_left.sum >=res_right.sum)
            return res_left;
        else if (res_right.sum >= res_left.sum && res_right.sum >= res_cross.sum)
            return res_right;
        else
            return res_cross;
        
    }
}
void calc_max(int *A, int count)
{
    struct result sum = sum_array(A, 0, count - 1);
    printf("left:%d,right:%d,sum:%d\n",sum.low, sum.high, sum.sum);
}