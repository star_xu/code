#include <stdio.h>
#include <malloc.h>
#include <string.h>
#define N 26
#define M 500
enum Color {
    white = 0,
    gray,
    black
};
struct Node {
    char vertex;
    struct Node *p;
    int color;
    int index;
};
struct Graph {
    struct Node node[N];
    char matrix[M][M];
    int node_num;
    int edge_num;
    int cycle;
    int not_sure;
};
struct stack {
    char data[N];
    int top;
};
static struct stack s;
void push(char u)
{
    s.data[s.top] = u;
    s.top++;
}
char pop()
{
    if (s.top == 0)
        return 0;
    s.top--;
    return s.data[s.top];
}
void dfs_visit(struct Graph *g, struct Node *u)
{
    u->color = gray;
    for (int j = 0; j < g->node_num; j++) {
        if (g->matrix[u->index][j] == 1) {
            struct Node *v = &g->node[j];
            if (v->color == white) {
                v->p = u;
                dfs_visit(g, v);
            } else if (v->color == gray) {
                g->cycle = 1;
                return;
            }
        }
    }
    u->color = black;
    push(u->vertex);
}
int check(struct Graph *g)
{
    int sum = 0;
    for (int i = 0; i < g->node_num; i++) {
        if (g->node[i].p == NULL) {
                sum++;
                if (sum > 1)
                    return 1;
        }
    }
    return 0;
}
void dfs(struct Graph *g)
{
    struct Node *u;
    for (int i = 0; i < g->node_num; i++) {
        u = &g->node[i];
        if (u->color == white && u->vertex != 0)
            dfs_visit(g, u);
    }
}
#define BUF_SIZE 4
static char buf[BUF_SIZE];
static struct Graph temp;
int main()
{
    struct Graph *g = malloc(sizeof(struct Graph));
    if (g == NULL)
        return -1;
    memset(g, 0, sizeof(struct Graph));

    printf("please input node num and edge num:\n");
    scanf("%d%d", &g->node_num, &g->edge_num);
    for(int i = 0; i < g->edge_num; i++) {
        int u, v;
        scanf("%s", buf);
        u = buf[0] - 'A';
        v = buf[2] - 'A';
        g->node[u].vertex = buf[0];
        g->node[u].index = u;

        g->node[v].vertex = buf[2];
        g->node[v].index = v;
        g->node[v].p = &g->node[u];
        g->matrix[u][v] = 1;
        if (g->matrix[v][u] == 1) {
            printf("inconsistency found after %d relations.\n", (i + 1));
            return 0;
        }
        memcpy(&temp, g, sizeof(struct Graph));
        memset(&s, 0 ,sizeof(struct stack));
        dfs(&temp);
        temp.not_sure = check(&temp);
        if (temp.cycle == 1) {
            printf("inconsistency found after %d relations.\n", (i + 1));
            return 0;
        }
        if (s.top == g->node_num) {
            if (!temp.not_sure) {
                printf("Sorted sequence determined after %d relations: ", (i + 1));
                while(s.top) {
                    printf("%c", pop());
                }
                printf(".\n");
                return 0;
            } else {
                continue;
            }
        }
    }
    printf("Sorted sequence cannot be determined.\n");
}