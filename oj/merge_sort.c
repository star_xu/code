#include <stdio.h>
#include "merge_sort.h"
#include <malloc.h>
#include <string.h>
#include "utils.h"
void do_merge_sort(int *a, int p, int q, int c)
{
    printf("p:%d,q:%d,r:%d\n",p,q,c);
    int n1 = q - p + 1;
    int n2 = c - q;
    int i = 0;
    int *l = malloc((n1 + 1 )* sizeof(int));
    if (l == NULL)
        printf("malloc left buffer error\n");
    memset(l, 0 , (n1  + 1)* sizeof(int));
    int *r = malloc((n2 + 1) * sizeof(int));
    if (r == NULL)
        printf("malloc right buffer error\n");
    memset(r, 0 , (n2 + 1) * sizeof(int));
    for (i = 0;i < q - p + 1; i++)
        l[i] = a[p+i];
    array_output(l, q - p + 1);
    l[i] = 0x101;
    for (i = 0; i < c - q ; i++)
        r[i] = a[q+ i + 1];
    array_output(r, c - q );
    r[i] = 0x101;
    int m = 0;
    int n = 0;
    for (i = p; i <= c; i++) {
        if (l[m] < r[n]) {
            a[i] = l[m];
            m++;
        } else {
            a[i] = r[n];
            n++;
        }
    }
}
static void merge_sort1(int *a, int p, int r)
{
    int q;
    if (p < r) {
        q= (p + r)/2;
        merge_sort1(a, p, q);
        merge_sort1(a, q + 1, r);
        do_merge_sort(a, p , q, r);
    }
}
void merge_sort(int *p, int count)
{
    merge_sort1(p, 0, count - 1);
    //do_merge_sort(p, 0, count/2, count - 1);
    printf("merge sort\n");
}